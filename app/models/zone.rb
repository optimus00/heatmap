class Zone < ActiveRecord::Base
  #Location type 0 -> railroads, type 1 -> accidents, type 2 -> schools
  attr_accessible :location, :location_type
end
