class ZonesController < ApplicationController
  def index
    render "index"
  end


  # POST /zones
  # POST /zones.json
  def create
    

    geofactory = RGeo::Cartesian.simple_factory(:srid => 4326)

    logger.debug params

    begin
      coordinates = RGeo::GeoJSON.decode(params["bounds"], :json_parser => :json)
    rescue
      render :json => {:success => false, :error => "Error in input format"}
      return
    end

    points = Array.new

    coordinates.each do |coordinate|
      points << coordinate.geometry
    end

    logger.debug geofactory.line_string(points)

    zones = Zone.where("ST_Intersects(location, box2d(ST_GeomFromText('#{geofactory.line_string(points)}')))")

    outpoints = Array.new

    zones.each do |zone|
      outpoints << zone.location
    end

    render :json => {:success => true, :points => RGeo::GeoJSON.encode(geofactory.multi_point(outpoints))}

  end

end
